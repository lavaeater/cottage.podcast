﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace cottage.auth
{
    public static class CottageAuthExtensions
    {
        public static void AddCottageAuth(this IServiceCollection services)
        {
            services.AddDefaultIdentity<IdentityUser>()
                .AddRoles<IdentityRole>()
                .AddRoleStore<IdentityUserStore>()
                .AddUserStore<IdentityUserStore>();

        }
    }
}
