using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace cottage.auth
{
    public class IdentityUserStore : IUserEmailStore<IdentityUser>, IUserLoginStore<IdentityUser>, IUserRoleStore<IdentityUser>, IRoleStore<IdentityRole>, IRoleValidator<IdentityRole>
    {
        private static IdentityUser _tommie = new IdentityUser()
        {
            AccessFailedCount = 0,
            Email = "tommie.nygren@gmail.com",
            EmailConfirmed = true,
            Id = "105341126505462833220",
            LockoutEnabled = false,
            TwoFactorEnabled = false,
            UserName = "Tommie Nygren",
            NormalizedEmail = "TOMMIE.NYGREN@GMAIL.COM",
            NormalizedUserName = "TOMMIE NYGREN"
        };

        private static IdentityRole _adminRole = new IdentityRole("Admin");

        private static IDictionary<IdentityUser, IList<IdentityRole>> _usersAndRoles =
            new Dictionary<IdentityUser, IList<IdentityRole>>()
            {
                {
                    _tommie, new List<IdentityRole>()
                    {
                        _adminRole
                    }
                }
            };

        private static IDictionary<IdentityRole, IList<IdentityUser>> _rolesAndUsers =
            new Dictionary<IdentityRole, IList<IdentityUser>>()
            {
                {
                    _adminRole, new List<IdentityUser>()
                    {
                        _tommie
                    }
                }
            };

        private static IList<IdentityUser> _allUsers = new List<IdentityUser>()
        {
            _tommie
        };

        public void Dispose()
        {
        }

        public Task<IdentityResult> CreateAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            if (!_allUsers.Any(u => u.Email == user.Email))
            {
                _allUsers.Add(user);
            }

            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            if (_allUsers.Any(u => u.Email == user.Email))
            {
                var uToRemove = _allUsers.First(u => u.Email == user.Email);
                _allUsers.Remove(uToRemove);

            }
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> CreateAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> DeleteAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        Task<IdentityRole> IRoleStore<IdentityRole>.FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        Task<IdentityRole> IRoleStore<IdentityRole>.FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetNormalizedRoleNameAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetRoleIdAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetRoleNameAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetNormalizedRoleNameAsync(IdentityRole role, string normalizedName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetRoleNameAsync(IdentityRole role, string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return Task.FromResult(_allUsers.FirstOrDefault(u => u.Id == userId));
        }

        public Task<IdentityUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetNormalizedUserNameAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetUserIdAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id);
        }

        public Task<string> GetUserNameAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public Task SetNormalizedUserNameAsync(IdentityUser user, string normalizedName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetUserNameAsync(IdentityUser user, string userName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            return Task.FromResult(_allUsers.FirstOrDefault(u => u.Email.ToUpperInvariant() == normalizedEmail));
        }

        public Task<string> GetEmailAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(_allUsers.FirstOrDefault(u => u.Email == user.Email)?.EmailConfirmed == true);
        }

        public Task<string> GetNormalizedEmailAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email.ToUpperInvariant());
        }

        public Task SetEmailAsync(IdentityUser user, string email, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetNormalizedEmailAsync(IdentityUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task AddLoginAsync(IdentityUser user, UserLoginInfo login, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityUser> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            return Task.FromResult(_allUsers.FirstOrDefault(u => u.Id == providerKey));
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveLoginAsync(IdentityUser user, string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task AddToRoleAsync(IdentityUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(IdentityUser user, CancellationToken cancellationToken)
        {
            var actualUser = _allUsers.FirstOrDefault(u => u.Email == user.Email);
            if (actualUser != null)
                return Task.FromResult(_usersAndRoles[actualUser].Select(r => r.Name).ToList() as IList<string>);
            else
                return Task.FromResult(new List<string>() as IList<string>);
        }

        public Task<IList<IdentityUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            var actualRole = _rolesAndUsers.Keys.FirstOrDefault(r => r.Name == roleName);
            if (actualRole != null)
            {
                return Task.FromResult(_rolesAndUsers[actualRole]);
            }
            else
            {
                return Task.FromResult(new List<IdentityUser>() as IList<IdentityUser>);
            }

        }

        public Task<bool> IsInRoleAsync(IdentityUser user, string roleName, CancellationToken cancellationToken)
        {
            var actualRole = _rolesAndUsers.Keys.FirstOrDefault(r => r.Name == roleName);
            if (actualRole != null)
            {
                return Task.FromResult(_rolesAndUsers[actualRole].Any(u => u.Id == user.Id));
            }

            return Task.FromResult(false);
        }

        public Task RemoveFromRoleAsync(IdentityUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> ValidateAsync(RoleManager<IdentityRole> manager, IdentityRole role)
        {
            return Task.FromResult(IdentityResult.Success);
        }
    }
}