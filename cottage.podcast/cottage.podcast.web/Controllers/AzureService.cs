﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;

namespace cottage.podcast.web.Controllers
{
    public class AzureService : IAzureService
    {
        private const string PostsTableName = "posts";
        private const string IncrementedTable = "incrementedid";
        private const string BlobRef = "media";
        private readonly IConfiguration _configuration;

        public AzureService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private CloudTableClient TableClient =>
            new Lazy<CloudTableClient>(() => StorageAccount.CreateCloudTableClient()).Value;

        private CloudStorageAccount StorageAccount =>
            CloudStorageAccount.Parse(_configuration.GetSection("ConnectionStrings").GetValue<string>("storage"));

        private CloudBlobClient BlobClient =>
            new Lazy<CloudBlobClient>(() => 
                StorageAccount.CreateCloudBlobClient()).Value;

        private CloudBlobContainer BlobContainer =>
            new Lazy<CloudBlobContainer>(() => 
                BlobClient.GetContainerReference(BlobRef)).Value;

        private async Task<long> GetNextId()
        {
            var idTable = TableClient.GetTableReference(IncrementedTable);
            await idTable.CreateIfNotExistsAsync();

            var id = await idTable.ExecuteAsync(TableOperation.Retrieve<IncrementedId>(PartitionKeys.IncrementedId, PartitionKeys.IncrementedId));
            var etag = id.Etag;
            int maxTries = 3;

            if (id.Result is IncrementedId idEnt)
            {
                idEnt.ETag = etag;
            }
            else
            {
                idEnt = new IncrementedId()
                {
                    PartitionKey = PartitionKeys.IncrementedId,
                    RowKey = PartitionKeys.IncrementedId
                };
            }

            idEnt.Id++;
            bool failed = true;
            int tries = 0;

            while (failed && tries < maxTries)
            {
                try
                {
                    await idTable.ExecuteAsync(TableOperation.InsertOrReplace(idEnt));
                    failed = false;
                }
                catch (StorageException ex)
                {
                    if (ex.RequestInformation.HttpStatusCode == 412 && tries < maxTries)
                        tries++;
                    else
                        throw;
                }
            }
            return idEnt.Id;
        }

        public async Task InsertBlogPost(Post post)
        {
            post.Id = await GetNextId();
            post.AuthorEmail = "";

            var postsTable = TableClient.GetTableReference(PostsTableName);
            await postsTable.CreateIfNotExistsAsync();

            await postsTable.ExecuteAsync(TableOperation.Insert(post));
        }

        public async Task<string> GetSasToken()
        {
            if (await BlobContainer.CreateIfNotExistsAsync())
            {
                await BlobContainer.SetPermissionsAsync(new BlobContainerPermissions
                    { PublicAccess = BlobContainerPublicAccessType.Blob });
            }
            
            return BlobContainer.GetSharedAccessSignature(new SharedAccessBlobPolicy()
            {
                Permissions = SharedAccessBlobPermissions.Create | SharedAccessBlobPermissions.Write,
                SharedAccessExpiryTime = DateTimeOffset.Now.AddHours(1),
                SharedAccessStartTime = DateTimeOffset.Now
            });
        }

        /// <summary>
        /// Only needed if the wrong api version is set.
        /// One-time-use only.
        /// </summary>
        /// <returns></returns>
        public async Task FixBlobApiVersion()
        {
            var props = await BlobClient.GetServicePropertiesAsync();
            props.DefaultServiceVersion = "2014-02-14";
            await BlobClient.SetServicePropertiesAsync(props);
        }

        public async Task InsertEpisode(Post podcastEpisode)
        {
            podcastEpisode.Id = await GetNextId();

            var blob = new CloudBlockBlob(new Uri(podcastEpisode.BlobRef), BlobClient);

            await blob.FetchAttributesAsync();

            podcastEpisode.BlobLength = blob.Properties.Length;
            podcastEpisode.EpisodeDuration = blob.Properties.Length * 8 / 150000;

            var postsTable = TableClient.GetTableReference(PostsTableName);
            await postsTable.CreateIfNotExistsAsync();

            await postsTable.ExecuteAsync(TableOperation.Insert(podcastEpisode));
        }

        public async Task<IEnumerable<Post>> ListPosts()
        {
            var postsTable = TableClient.GetTableReference(PostsTableName);
            await postsTable.CreateIfNotExistsAsync();

            var posts = await postsTable.ExecuteQuerySegmentedAsync(new TableQuery<Post>(), null);

            return posts.Results;
        }

    }
}