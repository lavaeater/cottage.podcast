﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Xml;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;

namespace cottage.podcast.web.Controllers
{
    [ApiController]
    public class PodcastController : ControllerBase
    {
        private readonly IAzureService _azureService;
        private readonly string _siteUrl;

        public PodcastController(
            IAzureService azureService,
            IConfiguration configuration)
        {
            _azureService = azureService;
            _siteUrl = configuration.GetSection("SiteConfig").GetValue<string>("SiteUrl");
        }

        [ResponseCache(Duration = 1200)]
        [HttpHead("/feed.xml")]
        public async Task<IActionResult> Head()
        {
            var posts = await _azureService.ListPosts();
            return Ok(
                    posts
                        .Where(p => p.IsEpisode)
                        .OrderByDescending(p => p.PublishAt)
                        .ToList())
                .ForceAsXml(_siteUrl, false);
        }

        [ResponseCache(Duration = 1200)]
        [HttpGet("/feed.xml")]
        public async Task<ActionResult<IEnumerable<Post>>> Get()
        {
            var posts = await _azureService.ListPosts();
            return Ok(
                posts
                    .Where(p => p.IsEpisode && p.PublishAt < DateTime.Now)
                    .OrderByDescending(p => p.PublishAt)
                    .ToList())
                .ForceAsXml(_siteUrl, true);
        }
    }

    public static class FeedResponseExtensions
    {
        public static T ForceAsXml<T>(this T result, string siteUrl, bool writeBody)
            where T : ObjectResult
        {
            result.Formatters.Clear();
            result.Formatters.Add(new RssFeedOutputFormatter(siteUrl, writeBody));

            return result;
        }
    }

    public class RssFeedOutputFormatter : IOutputFormatter
    {
        private const string TommieTheBrainInSpace = "Tommie the Brain - in Space";
        private readonly string _siteUrl;
        private readonly bool _writeBody;

        private readonly string _description =
            @"Innerst inne har du alltid känt att du är speciell. Att du, just du, har något särskilt att
erbjuda världen och dina medmänniskor. Det kan vara bra att veta att du har helt fel och 
att du vare sig är speciell eller någonsin kommer bidra med något av värde till världen i stort.";

        public RssFeedOutputFormatter(string siteUrl, bool writeBody)
        {
            _siteUrl = siteUrl;
            _writeBody = writeBody;
        }
        public bool CanWriteResult(OutputFormatterCanWriteContext context)
        {
            return context.ObjectType == typeof(List<Post>);
        }

        public async Task WriteAsync(OutputFormatterWriteContext context)
        {
            var posts = (context.Object as IEnumerable<Post>)?.ToList();
            var memoryStream = new MemoryStream();
            var writer = new XmlTextWriter(memoryStream, Encoding.UTF8);

            await WriteXml(writer, posts);

            memoryStream.Seek(0, SeekOrigin.Begin);

            if (_writeBody)
            {
                await memoryStream.CopyToAsync(context.HttpContext.Response.Body);
            }

            var lastUpdate = posts?.Select(p => p.PublishAt).Max();

            var length = memoryStream.Length;
            writer.Close();
            context.HttpContext.Response.Headers.Add("Content-Length", length.ToString());
            context.HttpContext.Response.Headers.Add("Last-Modified", lastUpdate?.ToString("R"));
            context.HttpContext.Response.Headers.Add("Content-Type", "application/rss+xml");

            context.ContentType = "application/rss+xml";
        }

        private Task WriteXml(XmlWriter writer, IEnumerable<Post> posts)
        {
            writer.WriteStartDocument();

            // The mandatory rss tag
            writer.WriteStartElement("rss");
            writer.WriteAttributeString("version", "2.0");

            writer.WriteAttributeString("xmlns:itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd");
            writer.WriteAttributeString("xmlns:googleplay", "http://www.google.com/schemas/play-podcasts/1.0");
            writer.WriteAttributeString("xmlns:atom", "http://www.w3.org/2005/Atom");
            writer.WriteAttributeString("xmlns:media", "http://search.yahoo.com/mrss/");
            writer.WriteAttributeString("xmlns:content", "http://purl.org/rss/1.0/modules/content/");

            // The channel tag contains RSS feed details
            writer.WriteStartElement("channel");
            writer.WriteStartElement("atom:link");
            writer.WriteAttributeString("href", $"{_siteUrl}/feed.xml");
            writer.WriteAttributeString("rel", "self");
            writer.WriteAttributeString("type", "application/rss+xml");
            writer.WriteEndElement();
            writer.WriteElementString("title", TommieTheBrainInSpace);
            writer.WriteElementString("link", _siteUrl);
            writer.WriteElementString("description", _description);
            writer.WriteElementString("copyright",
                $"Copyright {DateTime.Today.Year} Tommie Nygren. All rights reserved.");
            writer.WriteElementString("language", "sv-se");

            //Itunes bullshit
            writer.WriteElementString("itunes:type", "episodic");
            writer.WriteElementString("itunes:explicit", "no");

            writer.WriteElementString("itunes:subtitle", TommieTheBrainInSpace);
            writer.WriteElementString("itunes:author", "Tommie the Brain");
            writer.WriteElementString("itunes:summary", _description);
            writer.WriteStartElement("itunes:category");
            writer.WriteAttributeString("text", "Philosophy");
            writer.WriteEndElement();
            //Owner
            writer.WriteStartElement("itunes:owner");
            writer.WriteElementString("itunes:name", "Tommie Nygren");
            writer.WriteElementString("itunes:email", "tommie.nygren@gmail.com");
            writer.WriteEndElement();


            writer.WriteStartElement("itunes:image");
            writer.WriteAttributeString("href", $"{_siteUrl}/images/profile.jpg");
            writer.WriteEndElement();
            //End itunes bullshit

            // File Parade image    
            writer.WriteStartElement("image");
            writer.WriteElementString("url",
                $"{_siteUrl}/images/profile.jpg");
            writer.WriteEndElement();
            writer.WriteElementString("title",
              "Tommie The Brain - In Space");
            writer.WriteElementString("link",
                $"{_siteUrl}");

            // Loop through each item and add them to the RSS feed
            foreach (var post in posts)
            {

                writer.WriteStartElement("item");
                writer.WriteElementString("title", post.Title);
                writer.WriteElementString("description", post.Body);

                //itunes bullshit start
                writer.WriteElementString("itunes:episodeType", "full");
                writer.WriteElementString("itunes:episode", post.RowKey);
                writer.WriteElementString("itunes:duration", post.EpisodeDuration.ToString());
                writer.WriteElementString("itunes:explicit", "false");
                writer.WriteElementString("itunes:author", "Tommie the Brain");
                writer.WriteElementString("itunes:subtitle", "");
                writer.WriteStartElement("itunes:image");
                writer.WriteAttributeString("href", $"{_siteUrl}/images/profile.jpg");
                writer.WriteEndElement();
                //itunes stop

                writer.WriteStartElement("enclosure");
                writer.WriteAttributeString("length", post.BlobLength.ToString());
                writer.WriteAttributeString("type", "audio/mpeg");
                writer.WriteAttributeString("url", post.BlobRef);
                writer.WriteEndElement();
                writer.WriteElementString("guid", post.GuidString);
//                writer.WriteElementString("pubDate", post.PublishAt.ToString("").ToLongDateString());
                writer.WriteElementString("pubDate", post.PublishAt.ToString("R"));

                writer.WriteEndElement();
            }
            // Close all tags
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();

            return Task.CompletedTask;
        }
    }
}