﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace cottage.podcast.web.Controllers
{
    public interface IAzureService
    {
        Task<IEnumerable<Post>> ListPosts();
        Task InsertBlogPost(Post post);
        Task InsertEpisode(Post podcastEpisode);
        Task<string> GetSasToken();
        Task FixBlobApiVersion();
    }
}