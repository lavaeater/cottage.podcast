﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace cottage.podcast.web.Controllers
{
    public class IncrementedId : TableEntity
    {
        public IncrementedId()
        {
            PartitionKey = PartitionKeys.IncrementedId;
            RowKey = PartitionKeys.IncrementedId;
        }

        public long Id { get; set; }
    }

    public class Post : TableEntity
    {
        public Post()
        {
            Id = 0;
            IsEpisode = false;
            PartitionKey = PartitionKeys.Post;
        }

        public long Id
        {
            get => long.Parse(RowKey);
            set => RowKey = value.ToString();
        }

        public string Title { get; set; }
        public string Body { get; set; }
        public bool IsEpisode { get; set; }

        public string BlobRef { get; set; }

        public string AuthorEmail { get; set; }
        public string AuthorName { get; set; }
        public DateTime PublishAt { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public long BlobLength { get; set; } = 0;
        public string GuidString { get; set; } = Guid.NewGuid().ToString();
        public long EpisodeDuration { get; set; } = 0;
    }
}