﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace cottage.podcast.web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class   AdminController : Controller
    {
        private readonly IAzureService _azureService;

        private readonly IUserStore<IdentityUser> _userStore;

        
        public AdminController(IAzureService azureService, IUserStore<IdentityUser> userStore)
        {
            _azureService = azureService;
            _userStore = userStore;
        }

        // GET: Admin
        public async Task<ActionResult> Index()
        {
            return View(await _azureService.ListPosts());
        }

        // GET: Admin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm]Post post)
        {
            try
            {
                await _azureService.InsertBlogPost(post);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //Get: Admin/UploadEpisode
        public async Task<IActionResult> UploadEpisode()
        {
            ViewBag.SasToken = await _azureService.GetSasToken();
            return View();
        }

        // POST: Admin/UploadEpisode
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadEpisode([FromForm]PodcastEpisodeDto podcastEpisodeDto)
        {
            try
            {
                var podcastEpisode = new Post()
                {
                    IsEpisode = true,
                    AuthorEmail = podcastEpisodeDto.AuthorEmail,
                    AuthorName = podcastEpisodeDto.AuthorName,
                    Body = podcastEpisodeDto.Body,
                    Title = podcastEpisodeDto.Title,
                    PublishAt = new DateTime(
                        podcastEpisodeDto.PublishAt.Year, 
                        podcastEpisodeDto.PublishAt.Month,
                        podcastEpisodeDto.PublishAt.Day,
                        5,0,0),
                    BlobRef = podcastEpisodeDto.BlobRef
                };
                await _azureService.InsertEpisode(podcastEpisode);
                
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View();
            }
        }


    }
}