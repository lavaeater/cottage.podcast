﻿using System;

namespace cottage.podcast.web.Controllers
{
    public class PodcastEpisodeDto
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string AuthorEmail { get; set; }
        public string AuthorName { get; set; }
        public DateTime PublishAt { get; set; } = DateTime.Now;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public string BlobRef { get; set; }
    }
}