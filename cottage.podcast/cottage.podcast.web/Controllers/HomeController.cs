﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using cottage.podcast.web.Models;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Internal.Account;

namespace cottage.podcast.web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAzureService _azureService;

        public HomeController(
            ILogger<HomeController> logger, 
            IAzureService azureService)
        {
            _logger = logger;
            _azureService = azureService;
        }

        public async Task<IActionResult> Index()
        {
            var posts = (await _azureService.ListPosts()).Where(p => p.PublishAt < DateTime.Now);

            foreach (var post in posts)
            {
                post.Body = Markdig.Markdown.ToHtml(post.Body);
            }
            return View(posts);
        }

        [ResponseCache(Duration = 1200)]
        [HttpGet("/podcast")]
        public async Task<IActionResult> Podcast()
        {
            var episodes = (await _azureService.ListPosts()).Where(p => p.IsEpisode && p.PublishAt < DateTime.Now).OrderByDescending(p=> p.PublishAt);// && p.PublishAt < DateTime.Now);
            foreach (var episode in episodes)
            {
                episode.Body = Markdig.Markdown.ToHtml(episode.Body);
            }

            return View(episodes);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
