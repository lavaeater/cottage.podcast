﻿namespace cottage.podcast.web.Controllers
{
    public class PartitionKeys
    {
        public const string Post = "post";
        public const string IncrementedId = "incrementedid";
    }
}